/*REXX*/                                                       
/* CLEAR CACHE */                                              
  DO RUN_I = 1 BY 1 TO 12                                      
     "ISPEXEC CONTROL NONDISPL END"                            
     "ISPEXEC DISPLAY PANEL(ISR003"||RIGHT(RUN_I,2,'0')||")"   
  END                                                          
"ISPEXEC CONTROL DISPLAY REFRESH"                              
/* VARIABLES   */                                              
AUTHOR="MATHIEU.E"                                             
/* ISPF LIB    */                                              
"ISPEXEC LIBDEF ISPPLIB DATASET ID ('HLQ.ISPF.ISPPLIB')"       
                                                               
/* *********** */                                              
/* ISPF PANEL1 */                                              
/* *********** */                                              
"ISPEXEC DISPLAY PANEL (APP1$P01)"                             
"ISPEXEC VGET (IDE)"                                           
                                                               
/* *********** */                                              
/* ISPF PANEL2 */                                              
/* *********** */                                              
"ISPEXEC LIBDEF ISPTLIB DATASET ID('HLQ.ISPF.ISPTLIB')"        
"ISPEXEC TBOPEN TAPP1 WRITE"                                   
/* SEARCH ONE ROW */                                           
"ISPEXEC TBTOP TAPP1"                                          
IDENT = IDE                                                    
"ISPEXEC TBSARG TAPP1 NEXT NAMECOND(IDENT,EQ)"                 
"ISPEXEC TBSCAN TAPP1"                                         
"ISPEXEC TBGET  TAPP1"                                         
"ISPEXEC TBCLOSE TAPP1"                                        
IF NAME = "NAME" THEN NAME = "NOT FOUND"                       
CNAME = NAME                                                   
"ISPEXEC DISPLAY PANEL (APP1$P02)"                             
EXIT                                                           
